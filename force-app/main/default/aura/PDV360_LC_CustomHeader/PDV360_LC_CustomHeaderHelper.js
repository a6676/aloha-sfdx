({
	getlistFields : function(component) {
        var getListFiedlsAction = component.get("c.getListFiedls");
        var ObjectAPI = component.get("v.ObjectAPI");
        var FieldSetName = component.get("v.FieldSetName");
        var ObjectId = component.get("v.recordId");
        getListFiedlsAction.setParams({
            ObjectType : ObjectAPI,
            FieldSetName : FieldSetName,
            recordId: ObjectId
        });
        getListFiedlsAction.setCallback(this, function(response) {
            var state = response.getState();
            console.log('FieldSetController getListFiedls callback');
            console.log("callback state: " + state);
            
            if (state === "SUCCESS") {
                var form = response.getReturnValue();
                component.set('v.Objet', form.obj);
                component.set('v.fields', form.Fields);
                console.log("Objet " +form.obj);
                console.log("Objet " + component.get('v.Objet'));
            }
        });
        $A.enqueueAction(getListFiedlsAction);
    }
})