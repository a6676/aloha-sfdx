({
	fetchPickListVal: function(component,FieldAPI) {
        var action = component.get("c.getselectOptions");
        action.setParams({
            "fld": FieldAPI
        });
        var opts = [];
        action.setCallback(this, function(response) {
            if (response.getState() == "SUCCESS") {
                var allValues = response.getReturnValue();
 
                if (allValues != undefined && allValues.length > 0) {
                    opts.push({
                        class: "optionClass",
                        label: "--- Non filtre ---",
                        value: ""
                    });
                }
                for (var i = 0; i < allValues.length; i++) {
                    opts.push({
                        class: "optionClass",
                        label: allValues[i],
                        value: allValues[i]
                    });
                }
                component.find('accIndustry').set("v.options", opts);
            }
        });
        $A.enqueueAction(action);
    },
    searchRecordsHelper : function(component, event, helper) {
		$A.util.removeClass(component.find("Spinner"), "slds-hide");
        var field = component.get("v.field");
        component.set('v.message','');
        component.set('v.recordsList',null);
		// Calling Apex Method
    	var action = component.get('c.fetchRecords');
        action.setStorable();
        action.setParams({
            'APIField' : field.APIName,
            'searchString' : component.get('v.searchString'),
            'FieldType' : field.Type
        });
        action.setCallback(this,function(response){
        	var result = response.getReturnValue();
        	if(response.getState() === 'SUCCESS') {
                // To check if any records are found for searched keyword
    			if(result.length > 0) {
                    // To check if value attribute is prepopulated or not
                    component.set('v.recordsList',result);        
    			} else {
    				component.set('v.message','No Records Found');
    			}
        	} else if(response.getState() === 'INCOMPLETE') {
                component.set('v.message','No Server Response or client is offline');
            } else if(response.getState() === 'ERROR') {
                // If server throws any error
                var errors = response.getError();
                if (errors && errors[0] && errors[0].message) {
                    component.set('v.message', errors[0].message);
                }
            }
            // To open the drop down list of records
            $A.util.addClass(component.find('resultsDiv'),'slds-is-open');
        	$A.util.addClass(component.find("Spinner"), "slds-hide");
        });
        $A.enqueueAction(action);
	}
})