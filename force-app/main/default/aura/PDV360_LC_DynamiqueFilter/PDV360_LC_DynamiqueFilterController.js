({
    doInit : function(component, event, helper) {
        var field = component.get("v.field");
        if(field.Type == 'PICKLIST' || field.Type == 'MULTIPICKLIST')
            helper.fetchPickListVal(component,field.APIName);
        if(field.Type == 'REFERENCE' || field.APIName == 'Name')
        {
            $A.util.toggleClass(component.find('resultsDiv'),'slds-is-open');
        }
    },
    onCheck : function(component, event, helper) {
        var filterQuery = component.get("v.filterQuery");
        var field = component.get("v.field");
        var isChecked = component.find("checkboxDisplay").get('v.value');
        
        var obj = {};
        obj['Value'] = isChecked;
        obj['Condition'] = '=';
        filterQuery[field.APIName]=  obj;
        $A.util.removeClass(component.find("FiltreOnCheckbox"), "slds-hide");
        component.set("v.filterQuery",filterQuery);
    },
    removeFiltre: function(component, event, helper) {
        var filterQuery = component.get("v.filterQuery");
        var field = component.get("v.field");
        delete filterQuery[field.APIName];
        component.set("v.filterQuery",filterQuery);
        component.find("checkboxDisplay").set('v.value',false);
        $A.util.addClass(component.find("FiltreOnCheckbox"), "slds-hide");
    },
    onPicklistChange : function(component, event, helper) {
        var filterQuery = component.get("v.filterQuery");
        var field = component.get("v.field");
        var Picklistvalue = component.find("accIndustry").get('v.value');
        if(Picklistvalue =='')
        {
            delete filterQuery[field.APIName];
        }else{
            if(field.Type == 'PICKLIST' )
            {
                var obj = {};
                obj['Value'] = ' \'' + Picklistvalue + '\'';
                obj['Condition'] = '=';
                filterQuery[field.APIName]=  obj;
            }
            else if(field.Type == 'MULTIPICKLIST')
            {
                var obj = {};
                obj['Value'] = ' (\'' + Picklistvalue + '\')';
                obj['Condition'] = 'INCLUDES';
                filterQuery[field.APIName]=  obj;
            }
        }
        component.set("v.filterQuery",filterQuery);
    },
    onDateChange: function( component, event, helper ) {
        var filterQuery = component.get("v.filterQuery");
        var field = component.get("v.field");
        var filterOperator = component.find("filterOperator").get('v.value');
        var filterdate = component.find("filterdate").get('v.value');
        if(filterdate == null || filterdate == "")
        {
            delete filterQuery[field.APIName];
        }else{
            var obj = {};
            if(field.Type == 'DATETIME')
                obj['Value'] = '' + filterdate + 'T00:00:00Z' ;
            else
           		obj['Value'] = '' + filterdate ;
            obj['Condition'] = filterOperator;
            filterQuery[field.APIName]=  obj;
        }
        component.set("v.filterQuery",filterQuery);
    },
    onNumberChange: function( component, event, helper ) {
        var filterQuery = component.get("v.filterQuery");
        var field = component.get("v.field");
        var filterOperator = component.find("filterOperator").get('v.value');
        var filterNumber = component.find("filterNumber").get('v.value');
        if(filterNumber == null)
        {
            delete filterQuery[field.APIName];
        }else{
            var obj = {};
            obj['Value'] = ''+ filterNumber ;
            obj['Condition'] = filterOperator;
            filterQuery[field.APIName]=  obj;
        }
        component.set("v.filterQuery",filterQuery);
    },
    //Lookup Function
    // When a keyword is entered in search box
    searchRecords : function( component, event, helper ) {
        if( !$A.util.isEmpty(component.get('v.searchString')) && component.get('v.searchString').length>2 ) {
            helper.searchRecordsHelper( component, event, helper);
        } else {
            $A.util.removeClass(component.find('resultsDiv'),'slds-is-open');
        }
    },
    
    // When an item is selected
    selectItem : function( component, event, helper ) {
        if(!$A.util.isEmpty(event.currentTarget.id)) {
            var recordsList = component.get('v.recordsList');
            var index = recordsList.findIndex(x => x.value === event.currentTarget.id)
            if(index != -1) {
                var selectedRecord = recordsList[index];
            }
            component.set('v.selectedRecord',selectedRecord);
            component.set('v.value',selectedRecord.value);
            
            var filterQuery = component.get("v.filterQuery");
            var field = component.get("v.field");            
            var obj = {};
            obj['Value'] = ' \'' + selectedRecord.value + '\'';
            obj['Condition'] = '=';
            if(field.APIName == 'Name')
                filterQuery['Id']=  obj;
            else
                filterQuery[field.APIName]=  obj;
            component.set("v.filterQuery",filterQuery);
            
            $A.util.removeClass(component.find('resultsDiv'),'slds-is-open');
        }
    },
    
    // To remove the selected item.
    removeItem : function( component, event, helper ){
        component.set('v.selectedRecord','');
        component.set('v.value','');
        component.set('v.searchString','');
        
        var filterQuery = component.get("v.filterQuery");
        var field = component.get("v.field");
        if(field.APIName == 'Name')
            delete filterQuery['Id'];
        else
            delete filterQuery[field.APIName];
        
        component.set("v.filterQuery",filterQuery);
        setTimeout( function() {
            component.find( 'inputLookup' ).focus();
        }, 250);
    },
    
    // To close the dropdown if clicked outside the dropdown.
    blurEvent : function( component, event, helper ){
        $A.util.removeClass(component.find('resultsDiv'),'slds-is-open');
    },
    showList: function( component, event, helper ){
        var RecordLists = component.get("v.recordsList");
        if(RecordLists != null && RecordLists.length != null && RecordLists.length > 0)
        	$A.util.addClass(component.find('resultsDiv'),'slds-is-open');
    }
})