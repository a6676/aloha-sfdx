({
    doInit : function(component, event, helper) {
        //Action to initialise list of equipment
        var actionInitQueues = component.get('c.getQueues');
        actionInitQueues.setParams({
            "interventionId":component.get("v.recordId")
        });        
        actionInitQueues.setCallback(this, function(response) {
            var state = response.getState();
            console.log("actionInitQueues : " + state);
            if (component.isValid() && state === "SUCCESS") {
                var queues = response.getReturnValue();
                for (var key in queues ) {
                	component.set("v.queues",queues[key]);
                	component.find("queues").set("v.value",key);
                    console.log('key = ' + key);
                    console.log('queues = ' + queues[key] );
                }  
            }
        });
        
        //Action to get Intervention typologie
        var interQualification = component.get('c.interQualification');
        interQualification.setParams({
            "recordId":component.get("v.recordId")
        });
        interQualification.setCallback(this, function(response) {
            var state = response.getState();
            console.log("interQualification : "+state);
            if (component.isValid() && state === "SUCCESS") {
                component.set("v.interIsQualified",response.getReturnValue());
            }
        });
        
        
        //Action to get User profil
        var userIsAuthorized = component.get('c.userIsAuthorized');
        userIsAuthorized.setParams({
            "recordId":component.get("v.recordId")
        });
        userIsAuthorized.setCallback(this, function(response) {
            var state = response.getState();
            console.log("userIsAuthorized : "+state);
            if (component.isValid() && state === "SUCCESS") {
                component.set("v.userIsAuthorized",response.getReturnValue());
            }
        });
        
        
        $A.enqueueAction(actionInitQueues);
        $A.enqueueAction(interQualification);
        $A.enqueueAction(userIsAuthorized);
        console.log("actionInitQueues ok");
    },
    
    handleClick : function(component, event, helper){
    	console.log("handleClick");
        var queueId = component.find("queues").get("v.value");
        if (queueId == '--') {
        	component.set("v.error", true);
        	component.set("v.success", false);
            component.set("v.errorMessage", $A.get("$Label.c.AT001"));
            return;        	
        }
        var actionAssignerIntervention = component.get("c.assignerIntervention");
        console.log('queueId = ' + queueId);
        actionAssignerIntervention.setParams({
            "interventionId":component.get("v.recordId"),
            "queueId":queueId
        });
        actionAssignerIntervention.setCallback(this, function(response) {
            var state = response.getState();
            if (component.isValid() && state === "SUCCESS") {
            	component.set("v.success", true);
            	component.set("v.error", false);
            	component.set("v.successMessage", $A.get("$Label.c.AT002"));
            	$A.get('e.force:refreshView').fire();
            } 
            
            
            else if (response.getState() === "ERROR") {
                var message = '';
                var errors = response.getError();
                console.log("error IA : "+ errors);
                if (errors) {
                    for(var i=0; i < errors.length; i++) {
                        for(var j=0; errors[i].pageErrors && j < errors[i].pageErrors.length; j++) {
                            message += (message.length > 0 ? '\n' : '') + errors[i].pageErrors[j].message;
                            console.log('testMessagePage');
                            console.log(message);
                        }
                        if(errors[i].fieldErrors) {
                            for(var fieldError in errors[i].fieldErrors) {
                                var thisFieldError = errors[i].fieldErrors[fieldError];
                                for(var j=0; j < thisFieldError.length; j++) {
                                    message += (message.length > 0 ? '\n' : '') + thisFieldError[j].message;
                                    console.log('testMessageField');
                                    console.log(message);
                                }
                            }
                        }
                        if(errors[i].message) {
                            message += (message.length > 0 ? '\n' : '') + errors[i].message;
                        }
                    }
                } else {
                    message += (message.length > 0 ? '\n' : '') + 'Unknown error';
                }
                if(message.includes('obligatoire')){
                    message=$A.get("$Label.c.AT003");
                }
                
                console.log('message: '+message);
                component.set("v.error", true);
                component.set("v.success", false);
                component.set("v.errorMessage", message);
            }
            /*else {
            	component.set("v.error", true);
            	component.set("v.success", false);
            	component.set("v.errorMessage", $A.get("$Label.c.AT003"));
            }*/
        });
        $A.enqueueAction(actionAssignerIntervention);
    }
})