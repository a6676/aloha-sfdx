({
    InitMerch : function(component, event, helper) {
        var actions = [
            { label: 'Modifier', name: 'modifier' },
            { label: 'Supprimer', name: 'supprimer' }
        ];
        component.set("v.Columns", [
            {label: "Numero du merchandising", fieldName: "linkName", type: 'url', 
             typeAttributes: {label: { fieldName: "Name" }, target: "_blank"}},
            {label:"Concept", fieldName:"Concept_2020__c", type:"text"},
            {label:"Type de PLV", fieldName:"Type_de_PLV__c", type:"text"},
            {label:"Quantité", fieldName:"Quantite__c", type:"number"},
            { type: 'action', typeAttributes: { rowActions: actions }}
        ]);
        var action = component.get("c.getMerchandisings");
        action.setParams({recordId: component.get("v.recordId")});
        action.setCallback(this, function(data) {
            var state = data.getState();
            var NumOfLine = data.getReturnValue().length;
            if (state == "SUCCESS") {
                var records = data.getReturnValue();
                records.forEach(function(record){
                    record.linkName = '/'+record.Id;
                });
                component.set("v.Merchandisings", records);
                component.set("v.NbOfLine", NumOfLine);
            }
        });
        $A.enqueueAction(action);	
    },
    
    CreateRecordMerch: function(component, event, helper) {
        var recordId = component.get("v.recordId");
        var Concept = component.get("v.Account.Concept_2020__c");
        var createRecordEvent = $A.get("e.force:createRecord");
        createRecordEvent.setParams({
            "entityApiName": "PLV__c",
            "defaultFieldValues": {
                'Client__c' : recordId,
                'Concept_2020__c' : Concept        
            },
            "navigationLocation":"LOOKUP"
        });
        createRecordEvent.fire();
        //$A.get('e.force:refreshView').fire();
    },
    
    /*deleteMerchandising: function(component, event, helper) {
        
    }, */
    
    handleRowAction: function (component, event, helper) {
        var action = event.getParam('action');
        var row = event.getParam('row');
        switch (action.name) {
            case 'modifier':
                var editRecordEvent = $A.get("e.force:editRecord");
                editRecordEvent.fire({
                    "recordId":  row.Id
                });
                break;
            case 'supprimer':
                var action1 = component.get("c.deleteMerchandising");
                action1.setParams({"Merch": row});
                action1.setCallback(this, function(response) {
                    var state = response.getState();
                    if (state == "SUCCESS" ) {
                        var rows = component.get('v.Merchandisings');
                        var rowIndex = rows.indexOf(row);
                        rows.splice(rowIndex, 1);
                        component.set('v.Merchandisings', rows);
                    }
                });
                $A.enqueueAction(action1);
                break;
        }
    }
})