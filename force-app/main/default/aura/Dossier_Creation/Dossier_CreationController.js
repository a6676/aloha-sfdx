({
    doInit: function(component, event, helper) {
        console.log("DoInit Dossier Creation");
		var creerDossier = component.get('c.creerDossier');
        creerDossier.setParams({
            "accId" : component.get("v.recordId")
        });
        creerDossier.setCallback(this, function(reponse) {
            if(reponse.getReturnValue() != null){
                var caseId = reponse.getReturnValue();
                console.log('caseId = ' + caseId);
                var urlEvent = $A.get("e.force:navigateToURL");
                urlEvent.setParams({
                    "url": "/" + caseId
                });
                urlEvent.fire();  
            }
            else{
                component.set("v.profilIsAuthorized",false);
            }
        });
        //dismissActionPanel.fire();
        $A.enqueueAction(creerDossier);
    }
})