({
    doInit : function(component, event, helper) {
        
       console.log("doInit");
        
        //Action to initialise list of equipment
        var actionInitialiseEquipments = component.get('c.getMap');
        actionInitialiseEquipments.setParams({
            "recordId":component.get("v.recordId")
        });
        actionInitialiseEquipments.setCallback(this, function(response) {
            var state = response.getState();
            console.log("actionInitialiseEquipments : "+state);
            if (component.isValid() && state === "SUCCESS") {
                var equipementslist = response.getReturnValue();  
                component.set("v.MapOfEquipments",equipementslist);
                var equipements = new Array();  
                if(equipementslist != null ){
                    for (var key in equipementslist ) {
                        equipements.push(key);
                    }
                }
                equipements.push("Autres");
                component.set("v.equipements", equipements);
                component.find("equipements").set("v.value","--");
            }
        });
        
        //Action to get Case typologie
        var caseQualification = component.get('c.caseQualification');
        caseQualification.setParams({
            "recordId":component.get("v.recordId")
        });
        caseQualification.setCallback(this, function(response) {
            var state = response.getState();
            console.log("caseQualification : "+state);
            if (component.isValid() && state === "SUCCESS") {
                component.set("v.CaseIsQualified",response.getReturnValue());
            }
        });
        //Action to get Case typologie
        var caseOpen = component.get('c.caseIsOpen');
        caseOpen.setParams({
            "recordId":component.get("v.recordId")
        });
        caseOpen.setCallback(this, function(response) {
            var state = response.getState();
            console.log("caseOpen : "+state);
            if (component.isValid() && state === "SUCCESS") {
                component.set("v.CaseIsOpen",response.getReturnValue());
            }
        });
        
        //Action to get User profil
        var userIsAuthorized = component.get('c.userIsAuthorized');
        userIsAuthorized.setParams({
            "recordId":component.get("v.recordId")
        });
        userIsAuthorized.setCallback(this, function(response) {
            var state = response.getState();
            console.log("userIsAuthorized : "+state);
            if (component.isValid() && state === "SUCCESS") {
                component.set("v.userIsAuthorized",response.getReturnValue());
            }
        });
        
        $A.enqueueAction(actionInitialiseEquipments);
        $A.enqueueAction(caseQualification);
        $A.enqueueAction(caseOpen);
        $A.enqueueAction(userIsAuthorized);
        console.log("Dossier_Intervention DoInit ok");
    },
    
    handleClick : function(component,event,helper){
        console.log("createIntervention");
        var equipement = component.find("equipements").get("v.value");
        var equipements = component.get('v.MapOfEquipments');
        var equipementId="Autres";
 		if(!(equipement === "Autres")) equipementId = equipements[equipement].Id;
        if (equipement == '--') {
            component.set("v.error", true);
            component.set("v.success", false);
        }
        else {
            component.set("v.DOAButton", false);
            component.set("v.InterButton", false);
            var actionCreateIntervention = component.get("c.createIntervention");
            actionCreateIntervention.setParams({
                "DossierId":component.get("v.recordId"),
                "EquipementId":equipementId
            });
            actionCreateIntervention.setCallback(this, function(response) {
                var state = response.getState();
            	console.log("actionCreateIntervention : "+state);
                if (component.isValid() && state === "SUCCESS") {
                    var interventionId = response.getReturnValue();  
                    var navEvt = $A.get("e.force:navigateToSObject");
                    navEvt.setParams({
                        "recordId": interventionId,
                        "isredirect": false
                    });
                    navEvt.fire();
                }
            });
            $A.enqueueAction(actionCreateIntervention);
        }
        //"slideDevName": "related",
    },
    
    handleClickDOA : function(component,event,helper){
        console.log("createDOA");
        var equipement = component.find("equipements").get("v.value");
        var equipements = component.get('v.MapOfEquipments');
        var equipementId = equipements[equipement].Id;
        if (equipement == '--') {
            component.set("v.error", true);
            component.set("v.success", false);
        }
        else {
            component.set("v.DOAButton", false);
            component.set("v.InterButton", false);
            var actionCreateDOA = component.get("c.createDOA");
            actionCreateDOA.setParams({
                "DossierId":component.get("v.recordId"),
                "EquipementId":equipementId
            });
            actionCreateDOA.setCallback(this, function(response) {
                var state = response.getState();
            	console.log("actionCreateDOA : "+state);
                if (component.isValid() && state === "SUCCESS") {
                    var interventionId = response.getReturnValue();  
                    var navEvt = $A.get("e.force:navigateToSObject");
                    navEvt.setParams({
                        "recordId": interventionId,
                        "isredirect": false
                    });
                    navEvt.fire();
                }
            });
            $A.enqueueAction(actionCreateDOA);
        }
        //"slideDevName": "related",
    },
    
    
    
    onEquipementsChanges : function(component, event, helper) {
        var equipements = component.get("v.MapOfEquipments");
        var equipementname = component.find("equipements").get("v.value");
        if(equipementname == "--"){
            component.set("v.DOAButton", false);
            component.set("v.InterButton", false);
        } 
        else{
            if(equipementname == "Autres"){
                component.set("v.DOAButton", false);
                component.set("v.InterButton", true);
            }
            else{
                var equipement = equipements[equipementname];
                if(equipement.TECH_EligibleDOA__c == true){
                    component.set("v.DOAButton", true);
                    component.set("v.InterButton", false);
                }
                else{
                    component.set("v.DOAButton", false);
                    component.set("v.InterButton", true);            
                }
                console.log("equipement.TECH_EligibleDOA__c: "+equipement.TECH_EligibleDOA__c);
                console.log("component.get(v.DOAButton): "+component.get('v.DOAButton'));
            }
        }
    },
    
    
})