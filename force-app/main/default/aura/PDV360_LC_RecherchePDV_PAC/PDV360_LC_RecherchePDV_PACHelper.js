({
    getlistFields : function(component) {
        var getListFiedlsAction = component.get("c.getListFiedls");
        var pageSize = component.get("v.pageSize");
        var PACID = component.get("v.recordId");
        getListFiedlsAction.setParams({
            PACID : PACID
        });
        getListFiedlsAction.setCallback(this, function(response) {
            var state = response.getState();
            console.log('FieldSetController getListFiedls callback');
            console.log("callback state: " + state);
            
            if (state === "SUCCESS") {
                var form = response.getReturnValue();
                component.set('v.fields', form.Fields);
                component.set('v.paginationList', form.listAccounts);
                if(form.listAccounts.length < pageSize)
                    component.set('v.disableNextButton', true);
                else
                    component.set('v.disableNextButton', false);
            }
        });
        $A.enqueueAction(getListFiedlsAction);
    },
    getlistPDVs : function(component) {
        
        $A.util.removeClass(component.find("Spinner"), "slds-hide");
        var pageSize = component.get("v.pageSize");
        var filterQuery = component.get("v.filterQuery");
        var pagenumber = component.get("v.pagenumber");
        var PACID = component.get("v.recordId");
        
        var filter = "";
        for (var key in filterQuery){
            /*if(filter == "")
            {
                filter += "Where " + key + " " +  filterQuery[key].Condition + " " + filterQuery[key].Value;
            }
            else
            {*/
                filter += " AND " + key + " " +  filterQuery[key].Condition + " " + filterQuery[key].Value;
            /*}*/
        }
        console.log('filter ' + filter);
        
        var getListPDV = component.get("c.getListAccount");
        
        getListPDV.setParams({
            listFields:  component.get('v.fields'),
            WhereClause : filter,
            PageNumber : pagenumber,
            limitInt : pageSize,
            PACID : PACID
        });
        getListPDV.setCallback(this, function(response) {
            var state = response.getState();
            console.log('FieldSetController getListAccountAction callback');
            console.log("callback state: " + state);
            
            if (state === "SUCCESS") {
                var listAccount = response.getReturnValue();
                component.set('v.paginationList', listAccount);
                if(listAccount.length < pageSize)
                    component.set('v.disableNextButton', true);
                else
                    component.set('v.disableNextButton', false);
            }else {
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log('v.recordError',errors[0].message);
                    }
                } else {
                    console.log("Unknown error");
                }
            }
            $A.util.addClass(component.find("Spinner"), "slds-hide");
        });
        $A.enqueueAction(getListPDV);
    },
    getSelectedlist: function(component) {
        var getListselectedAccount = component.get("c.getListselectedAccount");
        var PACID = component.get("v.recordId");
        getListselectedAccount.setParams({
            PACId:  PACID
        });
        getListselectedAccount.setCallback(this, function(response) {
            var state = response.getState();
            console.log('FieldSetController getListselectedAccount callback');
            console.log("callback state: " + state);
            
            if (state === "SUCCESS") {
                var listPACPDV = response.getReturnValue();
                component.set('v.selectedList', listPACPDV);
                
            }else {
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log('v.recordError',errors[0].message);
                    }
                } else {
                    console.log("Unknown error");
                }
            }
        });
        $A.enqueueAction(getListselectedAccount);
    },
    createPACPDVhelper : function(component){
        var action = component.get("c.createPACPDV");
        var paginationList = component.get("v.paginationList");
        var PACID = component.get("v.recordId");
        action.setParams({
            PACId:  PACID,
            listAccRow:paginationList
        });
         action.setCallback(this, function(response) {
            var state = response.getState();
            console.log('FieldSetController createPACPDV callback');
            console.log("callback state: " + state);
            
            if (state === "SUCCESS") {
                var listPACPDV = response.getReturnValue();
                console.log('listPACPDV ' + listPACPDV);
                this.getSelectedlist(component);
                this.getlistPDVs(component);
            }else {
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log('v.recordError',errors[0].message);
                    }
                } else {
                    console.log("Unknown error");
                }
            }
        });
        $A.enqueueAction(action);
    },
    updatePACPDVhelper : function(component){
        $A.util.removeClass(component.find("Spinner"), "slds-hide");
        var action = component.get("c.updatePACPDV");
        var listPACPDV = component.get("v.selectedList");
        action.setParams({
            listPACPDV:listPACPDV
        });
         action.setCallback(this, function(response) {
            var state = response.getState();
            console.log('FieldSetController updatePACPDV callback');
            console.log("callback state: " + state);
            
            if (state === "SUCCESS") {
                this.getSelectedlist(component);
            }else {
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log('v.recordError',errors[0].message);
                    }
                } else {
                    console.log("Unknown error");
                }
            }
             $A.util.addClass(component.find("Spinner"), "slds-hide");
        });
        $A.enqueueAction(action);
    },
    deletePACPDVhelper : function(component){
        var actionDelete = component.get("c.deletePACPDV");
        var SelectedList = component.get('v.selectedList');
        actionDelete.setParams({
            SelectedList:  SelectedList
        });
        actionDelete.setCallback(this, function(response) {
            var state = response.getState();
            console.log('FieldSetController deletePACPDV callback');
            console.log("callback state: " + state);
            this.getSelectedlist(component);
            this.getlistPDVs(component);
        });
        
        $A.enqueueAction(actionDelete);
    },/*
    deletehelper : function(component,PAVPDVId){
        var actDelete = component.get("c.deletePACPDV");
        actDelete.setParams({
            PACPDVId:  PAVPDVId
        });
        actDelete.setCallback(this, function(response) {
            var state = response.getState();
            console.log('FieldSetController deletePACPDV callback');
            console.log("callback state: " + state);
            this.getSelectedlist(component);
        });
        
        $A.enqueueAction(actDelete);
    },
    deletehelper : function(component,event,helper){
        var PACPDVID = "a121x000000gFCEAA2"; //event.getSource().get("v.value");
        var action = component.get("c.deletePACPDV");
        action.setParams({
            PACPDVId:  PACPDVID
        });
         action.setCallback(this, function(response) {
            var state = response.getState();
            console.log('FieldSetController createPACPDV callback');
            console.log("callback state: " + state);
            
            if (state === "SUCCESS") {
                this.getSelectedlist(component);
            }else {
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log('v.recordError',errors[0].message);
                    }
                } else {
                    console.log("Unknown error");
                }
            }
        });
        $A.enqueueAction(action);
    }*/
})