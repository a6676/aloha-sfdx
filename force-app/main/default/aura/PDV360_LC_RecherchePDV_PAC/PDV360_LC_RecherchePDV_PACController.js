({
    doInit : function(component, event, helper) {
        helper.getlistFields(component);
        helper.getSelectedlist(component);
    },
    createPACPDVCtr: function(component, event, helper) {
        helper.createPACPDVhelper(component);
    },
    PageSizeChange : function(component, event, helper) {
        helper.getlistPDVs(component);
        component.find("SelectAllCheckbox").set('v.value',false);
    },
    onCheck : function(component, event, helper) {
        var paginationList = component.get("v.paginationList");
        var isChecked = component.find("SelectAllCheckbox").get('v.value');
        for (var i = 0; i < paginationList.length; i++) {
            paginationList[i].isSelected = isChecked;
        }
        component.set("v.paginationList",paginationList);
    },
    onCheckSelectAllPDVPAC : function(component, event, helper) {
        var selectedList = component.get("v.selectedList");
        var isChecked = component.find("SelectAllPDVPACCheckbox").get('v.value');
        for (var i = 0; i < selectedList.length; i++) {
            selectedList[i].isSelected = isChecked;
        }
        component.set("v.selectedList",selectedList);
    },
    ChangeFilter: function(component, event, helper) {
         component.set("v.pagenumber",0);
        helper.getlistPDVs(component);
        component.find("SelectAllCheckbox").set('v.value',false);
    },
    previousPage: function(component, event, helper) {
        var pagenumber = component.get("v.pagenumber");
        pagenumber--;
        component.set("v.pagenumber",pagenumber);
        helper.getlistPDVs(component);
        component.find("SelectAllCheckbox").set('v.value',false);
    },
    nextPage: function(component, event, helper) {
        var pagenumber = component.get("v.pagenumber");
        pagenumber++;
        component.set("v.pagenumber",pagenumber);
        helper.getlistPDVs(component);
        component.find("SelectAllCheckbox").set('v.value',false);
    },
    deletePACPDVController : function(component, event, helper) {
        helper.deletePACPDVhelper(component);
    },
    MAJPACPDV :function(component, event, helper) {
        helper.updatePACPDVhelper(component);
    },
     onCheckPreselctionner : function(component, event, helper) {
        var filterQuery = component.get("v.filterQuery");
        var isChecked = component.find("checkboxPreselectionner").get('v.value');
        
        var obj = {};
         obj['Value'] = ':setPDVPreSelectionner';
        if (isChecked)
            obj['Condition'] = 'IN';
         else
             obj['Condition'] = 'NOT IN';
        filterQuery['Code_Point_de_Vente__c']=  obj;
        $A.util.removeClass(component.find("FiltrePreSelectionner"), "slds-hide");
        component.set("v.filterQuery",filterQuery);
    },
    removeFiltrePreselectionnner: function(component, event, helper) {
        var filterQuery = component.get("v.filterQuery");
        delete filterQuery['Code_Point_de_Vente__c'];
        component.set("v.filterQuery",filterQuery);
        component.find("checkboxPreselectionner").set('v.value',false);
        $A.util.addClass(component.find("FiltrePreSelectionner"), "slds-hide");
    },
})