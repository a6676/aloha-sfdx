({
    
    saveRecord: function(component, event, helper) {
        component.set('v.Client.Date_de_mise_jour_FDRC__c', $A.localizationService.formatDate(new Date(), "YYYY-MM-DD"));
        component.find("recordClient").saveRecord($A.getCallback(function(saveResult) {
            if(saveResult.state === "SUCCESS" || saveResult.state === "DRAFT") {
                console.log("Record Saved");
                
                for(var key in saveResult) {
                    var value = saveResult[key];
                     console.log(key  + " " + value);
                }
                component.set('v.recordSucess','Les fonds de roulement ont bien été enregistrés.');
                component.find('recordClient').reloadRecord(true);
                component.set('v.showEditTable',false);
            } else if (saveResult.state === "INCOMPLETE") {
                console.log("User is offline, device doesn't support drafts.");
                component.set('v.recordError', saveResult.error[0].message );
            } else if (saveResult.state === "ERROR") {
                console.log('Problem saving record, error: ' + JSON.stringify(saveResult.error));
                component.set('v.recordError', saveResult.error[0].message );
            } else {
                console.log('Unknown problem, state: ' + saveResult.state + ', error: ' + JSON.stringify(saveResult.error));
            	component.set('v.recordError',saveResult.error[0].message);
            }
        }));
    },
    editRecord: function(component, event, helper) {
        component.set('v.showEditTable',true);
        component.set('v.recordSucess','');
       // component.set('v.recordError','');
    },
    cancelRecord: function(component, event, helper) {
        component.set('v.showEditTable',false);
        //component.find('recordClient').reloadRecord(true);
        component.set('v.recordSucess','');
        //component.set('v.recordError','');
    },
    handleOnSubmit : function(component, event, helper) {
    event.preventDefault();
    var fields = event.getParam("fields");
    fields["Date_de_mise_jour_FDRC__c"] = $A.localizationService.formatDate(new Date(), "YYYY-MM-DD");
    component.find("recordClient").submit(fields);
	},
    handleOnSuccess : function(component, event, helper) {
        component.set('v.showEditTable',false);
        component.set('v.recordSucess','Les fonds de roulement ont bien été enregistrés.');
    }   
})