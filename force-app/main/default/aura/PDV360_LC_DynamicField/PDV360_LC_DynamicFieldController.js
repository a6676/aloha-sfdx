({
    doInit : function(component, event, helper) {
        var sobject = component.get('v.objName');
        var fieldName = component.get('v.fieldName');
        var fieldType = component.get('v.fieldType');
        var formatText = component.find("fielddata");
        //console.log('fieldName  ' + fieldName + ' fieldType ' + fieldType + ' sobject ' +  sobject[fieldName]);
        if(fieldType.includes('REFERENCE') && sobject[fieldName.replace("__c","__r")] != null) {
            formatText.set("v.value",sobject[fieldName.replace("__c","__r")]['Name']); //Here we are fetching data from parent field
        }
        else if(fieldType.includes('BOOLEAN') ){
            var checkbox = component.find("checkboxDisplay");
            checkbox.set("v.value",sobject[fieldName]);
        }else if(fieldType.includes('DATETIME')){
            var datetimeText = sobject[fieldName] +"";
            formatText.set("v.value",datetimeText.replace("T"," ").replace(".000Z","") );
        }else if(sobject[fieldName] != null){
            formatText.set("v.value",sobject[fieldName] +"" );
        }
    }
    
})