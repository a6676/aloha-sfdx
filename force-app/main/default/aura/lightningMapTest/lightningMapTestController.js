({
    doInit: function (component, event, helper) {
        // call getLocation apex class method 
        var action = component.get("c.getLocation");
        action.setParams({ accountId : component.get("v.recordId")});
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                // set mapMarkersData attribute values with 'accountLocationWrapper' data 
                component.set('v.mapMarkersData',response.getReturnValue());
                // set the mapCenter location.
                component.set('v.mapCenter', {
                   
                        Latitude : component.get("v.Account.Billinglatitude"),
                        Longitude: component.get("v.Account.BillingLongitude")
                    
                });
                // set map markers title  
                component.set('v.markersTitle', 'Google Office locations.');
                
                console.log('test mapCenter' +   component.get('v.mapCenter'));
            }
            else if (state === "INCOMPLETE") {
                // do something
            }
                else if (state === "ERROR") {
                    var errors = response.getError();
                    if (errors) {
                        if (errors[0] && errors[0].message) {
                            console.log("Error message: " + 
                                        errors[0].message);
                        }
                    } else {
                        console.log("Unknown error");
                    }
                }
        });
        $A.enqueueAction(action);
    }
})