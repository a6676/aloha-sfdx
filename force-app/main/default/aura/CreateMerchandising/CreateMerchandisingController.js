({
	 invoke: function(component, event, helper) {
   
   var AccountID = component.get("v.InputAccountID"); 
   var Concept = component.get("v.InputConcept"); 
   
   var CRE = $A.get("e.force:createRecord");
   CRE.setParams({
       "entityApiName" : "PLV__c",
       "defaultFieldValues" : {
           'Client__c' : AccountID,
           'Concept_2020__c' : Concept
       }
   });
   CRE.fire(); 
 },
})