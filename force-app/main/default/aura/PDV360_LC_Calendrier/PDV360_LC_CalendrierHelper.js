({
	getCalendrier : function(component) {
		var action = component.get("c.getWrapCalendrierDay");
        console.log("NumberOfmonths " + component.get("v.NumberOfmonths"));
        var test = component.get("v.NumberOfmonths");
        action.setParams({ accountId : component.get("v.recordId"),
                          NumberOfMonths : test});

        // Create a callback that is executed after 
        // the server-side action returns
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                // Alert the user with the value returned 
                // from the server
                component.set('v.CalenderWeeks',response.getReturnValue());            
            }
            else {
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log('v.recordError',errors[0].message);
                    }
                } else {
                    console.log("Unknown error");
                }
            }
        });
		$A.enqueueAction(action);
	}
})