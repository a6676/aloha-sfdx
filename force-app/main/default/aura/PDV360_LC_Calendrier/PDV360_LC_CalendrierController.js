({
	doInit : function(component, event, helper) {
		helper.getCalendrier(component);
	},
    nextMonth: function(component, event, helper) {
        var NumberMonth= component.get("v.NumberOfmonths");
        NumberMonth++;
        component.set("v.NumberOfmonths",NumberMonth);
		helper.getCalendrier(component);
	},
    previousMonth: function(component, event, helper) {
        var NumberMonth = component.get("v.NumberOfmonths");
        NumberMonth--;
        component.set("v.NumberOfmonths",NumberMonth);
		helper.getCalendrier(component);
	},
     goToday: function(component, event, helper) {
        component.set("v.NumberOfmonths",0);
		helper.getCalendrier(component);
	}
})