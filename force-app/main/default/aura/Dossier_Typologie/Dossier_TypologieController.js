({
    doInit : function(component, event, helper) {
        var context = component.get("v.context");
        var isQualified = false;
        var niveau1 = new Array();      
        var niveau2 = new Array();          
        var niveau3 = new Array(); 
        
        if(context === "case"){
            component.set("v.Title", "Qualification du dossier");
            component.set("v.Niveau1Label", "Famille");
            component.set("v.Niveau2Label", "Sous-Famille");
            component.set("v.Niveau3Label", "Typologie");
            component.set("v.Niveau1InputLabel", "-- Sélectionner une famille ---");
            component.set("v.Niveau2InputLabel", "-- Sélectionner une sous-famille ---");
            component.set("v.Niveau3InputLabel", "-- Sélectionner une typologie ---");
        }
        if(context === "intervention"){
            component.set("v.Title", "Qualification de l'intervention");
            component.set("v.Niveau1Label", "Type d'intervention");
            component.set("v.Niveau2Label", "Prestation");
            component.set("v.Niveau3Label", "Priorité");  
            component.set("v.Niveau1InputLabel", "-- Sélectionner un type d'intervention ---");
            component.set("v.Niveau2InputLabel", "-- Sélectionner une prestation ---");
            component.set("v.Niveau3InputLabel", "-- Sélectionner une priorité ---");          
        }
        
        //Action to initialise typo with case infos
        var actionInitialiseTypo = component.get('c.getrecordById');
        actionInitialiseTypo.setParams({
            "recordId":component.get("v.recordId"),
            "context":context
        });
        actionInitialiseTypo.setCallback(this, function(response) {
            var state = response.getState();
            if (component.isValid() && state === "SUCCESS") {
                var typoExistant = response.getReturnValue();  
                if(typoExistant != null ){
            		isQualified = true; 
                    var typomap = component.get("v.MapOfTypologies"); 
                    console.log("isQualified2");
                    console.log(isQualified);
                    if((typoExistant['Niveau1'] != null && typoExistant['Niveau2']  != null && typoExistant['Niveau3']  != null)
                       &&(typoExistant['Niveau1'] != "" && typoExistant['Niveau2']  != "" && typoExistant['Niveau3']  != "")){
                        
                        component.find("niveau1").set("v.value",typoExistant['Niveau1']);
                        
                        niveau2 = new Array(); 
                        for(var key2 in typomap[typoExistant['Niveau1']]){
                            niveau2.push(key2); 
                        }
                        component.set("v.niveau2", niveau2.sort());
                        component.find("niveau2").set("v.value",typoExistant['Niveau2']);
                        
                        niveau3 = new Array(); 
                        for(var key3 in typomap[typoExistant['Niveau1']][typoExistant['Niveau2']]){
                            niveau3.push(key3);
                        }
                        component.set("v.niveau3", niveau3.sort());
                        component.find("niveau3").set("v.value",typoExistant['Niveau3']);
                    }
                }
            }
        });
        
        
        var actionGetMap = component.get('c.getMap');
        actionGetMap.setParams({
            "context":component.get("v.context"),
            "recordId":component.get("v.recordId")
        });
        actionGetMap.setCallback(this, function(response) {
            
            if (component.isValid() && response.getState() === "SUCCESS") {
                var typomap = response.getReturnValue();  
                component.set("v.MapOfTypologies", typomap);   
                
                //loop through map returned from Server, populating famille,sousFamille,typologie as we go
                for (var key in typomap ) {
                    niveau1.push(key);
                    var niveau2List = typomap[key]
                    for(var key2 in niveau2List){
                        niveau2.push(key2);
                        var niveau3List = niveau2List[key2]
                        for(var key3 in niveau3List){
                            niveau3.push(key3);
                        } 
                    }
                }
                
                component.set("v.niveau1", niveau1.sort());
                //component.set("v.niveau2", niveau2);
                //component.set("v.niveau3", niveau3);
                $A.enqueueAction(actionInitialiseTypo);
            }
        });
        // Send action off to be executed
        $A.enqueueAction(actionGetMap);
        
        component.set("v.error", false);
        component.set("v.error", false);
        console.log("Init OK");
    },
    
    
    onNiveau1Changes : function(component, event, helper) {
        var myMap = component.get("v.MapOfTypologies");
        var niveau1 = component.find("niveau1").get("v.value");
        var niveau2 = new Array();          
        var niveau3 = new Array();    
        
        for (var key in myMap ) {
            if(key == niveau1){
                var niveau2List = myMap[key]
                for(var key2 in niveau2List){
                    niveau2.push(key2);
                    var niveau3List = niveau2List[key2]
                    for(var key3 in niveau3List){
                        //niveau3.push(key3);
                    }
                }
            }
        }
        component.find("niveau2").set("v.value","-- Sélectionner une prestation ---");
        component.set("v.niveau2", niveau2.sort());
        component.set("v.niveau3", niveau3.sort());
    },
    
    
    onNiveau2Changes : function(component, event, helper) {
        var myMap = component.get("v.MapOfTypologies");
        var niveau1 = component.find("niveau1").get("v.value");
        var niveau2 = component.find("niveau2").get("v.value");
        var niveau3 = new Array();    
        
        for (var key in myMap ) {
            if(key == niveau1){
                var niveau2List = myMap[key]
                for(var key2 in niveau2List){
                    if(key2 == niveau2){
                        var niveau3List = niveau2List[key2]
                        for(var key3 in niveau3List){
                            niveau3.push(key3);
                        }
                    }
                }
            }
        }
        component.set("v.niveau3", niveau3.sort());
    },
    
    handleClick : function(component,event,helper){
        console.log("Click");
        var myMap = component.get("v.MapOfTypologies");
        var niveau1 = component.find("niveau1").get("v.value");
        var niveau2 = component.find("niveau2").get("v.value");
        var niveau3 = component.find("niveau3").get("v.value");
        
        var actionUpdateRecord = component.get('c.UpdateRecord');
        if (niveau1 == '--' || niveau2 == '--' || niveau3 == '--') {
            component.set("v.error", true);
            component.set("v.success", false);
            var message = component.get("v.Niveau1Label")+", "+component.get("v.Niveau2Label")+" et "+component.get("v.Niveau3Label")+" doivent être renseignés." ;
            component.set("v.errorMessage", message);
        } else {
            component.set("v.error", false);
            var typo = myMap[niveau1][niveau2][niveau3];
        	console.log(typo);
        	console.log(component.get("v.recordId"));
        	console.log(component.get("v.context"));
            actionUpdateRecord.setParams({
                "recordId":component.get("v.recordId"),
                "typologieId":typo.Id,
                "context":component.get("v.context")
            });
            actionUpdateRecord.setCallback(this, function(response) {
                var message = '';
                console.log("actionUpdateRecord STATE: " + response.getState());
                if (component.isValid() && response.getState() === "SUCCESS") {
                    $A.get('e.force:refreshView').fire();
                    component.set("v.success", true);
                    $A.get("e.c:ModificationTypologie").fire();
                    $A.get("e.c:DossierEvent").fire();

                }
                
                
                else if (response.getState() === "ERROR") {
                var errors = response.getError();
                if (errors) {
                    for(var i=0; i < errors.length; i++) {
                        for(var j=0; errors[i].pageErrors && j < errors[i].pageErrors.length; j++) {
                            message += (message.length > 0 ? '\n' : '') + errors[i].pageErrors[j].message;
                        }
                        if(errors[i].fieldErrors) {
                            for(var fieldError in errors[i].fieldErrors) {
                                var thisFieldError = errors[i].fieldErrors[fieldError];
                                for(var j=0; j < thisFieldError.length; j++) {
                                    message += (message.length > 0 ? '\n' : '') + thisFieldError[j].message;
                                }
                            }
                        }
                        if(errors[i].message) {
                            message += (message.length > 0 ? '\n' : '') + errors[i].message;
                        }
                    }
                } else {
                    message += (message.length > 0 ? '\n' : '') + 'Unknown error';
                }
                console.log('message: '+message);
                component.set("v.error", true);
                component.set("v.success", false);
                component.set("v.errorMessage", message);
            }
                
                
            })
            $A.enqueueAction(actionUpdateRecord); 
            
        }
    },
    
})