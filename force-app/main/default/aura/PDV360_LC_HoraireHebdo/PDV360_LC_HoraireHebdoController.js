({
    doInit: function(cmp) {
        var action = cmp.get("c.getCalendrierbyAccountID");
        action.setParams({ accountId : cmp.get("v.recordId") });

        // Create a callback that is executed after 
        // the server-side action returns
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                // Alert the user with the value returned 
                // from the server
                console.log("From server Calendar Id: " + response.getReturnValue().Id);
				cmp.set('v.CalendarId',response.getReturnValue().Id);
                cmp.find('recordCalendar').reloadRecord(true);
                cmp.find('ViewCalendar').reloadRecord(true);
            }
            else {
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log('v.recordError',errors[0].message);
                    }
                } else {
                    console.log("Unknown error");
                }
            }
        });
		$A.enqueueAction(action);
    },
    saveRecord: function(component, event, helper) {
        component.set('v.Calendar.Date_MAJ_Horaires__c', $A.localizationService.formatDate(new Date(), "YYYY-MM-DD"));
        component.find("recordCalendar").saveRecord($A.getCallback(function(saveResult) {
            if(saveResult.state === "SUCCESS" || saveResult.state === "DRAFT") {
                console.log("Record Saved");
                component.set('v.recordSucess','Les horaires d\'ouverture ont bien été enregistrés.');
                component.find('recordCalendar').reloadRecord(true);
                component.find('ViewCalendar').reloadRecord(true);
                component.set('v.showEditTable',false);
            } else if (saveResult.state === "INCOMPLETE") {
                console.log("User is offline, device doesn't support drafts.");
                component.set('v.recordError', saveResult.error[0].message );
            } else if (saveResult.state === "ERROR") {
                console.log('Problem saving record, error: ' + JSON.stringify(saveResult.error));
                component.set('v.recordError', saveResult.error[0].message );
            } else {
                console.log('Unknown problem, state: ' + saveResult.state + ', error: ' + JSON.stringify(saveResult.error));
            	component.set('v.recordError',saveResult.error[0].message);
            }
        }));
    },
    editRecord: function(component, event, helper) {
        component.set('v.showEditTable',true);
        component.set('v.recordSucess','');
        component.set('v.recordError','');
    },
    cancelRecord: function(component, event, helper) {
        component.set('v.showEditTable',false);
        component.find('recordCalendar').reloadRecord(true);
        component.find('ViewCalendar').reloadRecord(true);
        component.set('v.recordSucess','');
        component.set('v.recordError','');
    }
})