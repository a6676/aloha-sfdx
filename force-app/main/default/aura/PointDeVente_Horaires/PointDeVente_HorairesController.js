({
	// Chargement ou création des Heures d'Ouvertures associées au Point de Vente
	doInit : function(component, event, helper) {
        console.log('doInit');
        // Création de l'action qui va appeler le controleur Apex
        var actionGetHOID = component.get('c.getHeuresOuvertures');
        
        actionGetHOID.setParams({
            "myId":component.get("v.recordId"),
            "context":component.get("v.context")
        });
        // Définition du callback pour traiter la réponse
        actionGetHOID.setCallback(this, function(response) {
            var state = response.getState();
            if (component.isValid() && state === "SUCCESS") {
                component.set("v.heureRecordId", response.getReturnValue());
                console.log(response.getReturnValue());
                console.log('retour = ' + component.get("v.heureRecordId"));
                component.find("recordHandler").reloadRecord();
                
            } else {
                console.log("Failed with state: " + state);
            }
        });
            $A.enqueueAction(actionGetHOID);
        
	},
    
	turnOffEditMode : function(component){
        component.set("v.editModeOn","false");
        component.set("v.showErrorMsg","false");
	},
    
	turnOnEditMode : function(component){
        component.set("v.editModeOn","true");
        component.set("v.showSuccesMsg","false");
	},
    
    handleSaveRecord: function(component, event, helper) {
        component.find("recordHandler").saveRecord($A.getCallback(function(saveResult) {
            // Gestion du retour de la sauvegarde de l'enregistrement 
            if (saveResult.state === "SUCCESS" || saveResult.state === "DRAFT") {
        		component.set("v.editModeOn","false");
                component.set("v.showErrorMsg","false");
                component.set("v.showSuccesMsg","true");
                console.log("Sauvegarde des heures d'ouvertures effectués.");
            } else {
                var message = '';
                var errors = saveResult.error;
                console.log("errors = " + errors);
                console.log("saveResult.error = " + saveResult.error);
                console.log("saveResult.state = " + saveResult.state);
                console.log("errors.length = " + errors.length);
                if (errors) {
                    for(var i=0; i < errors.length; i++) {
                    	if (errors[i].pageErrors) {
	                        for(var j=0; errors[i].pageErrors && j < errors[i].pageErrors.length; j++) {
	                            message += (message.length > 0 ? '\n' : '') + errors[i].pageErrors[j].message;
	                            message = errors[i].pageErrors[j].message;
	                        }
	                    }
                        if(errors[i].fieldErrors) {
                            for(var fieldError in errors[i].fieldErrors) {
                            	if (fieldError) {
	                                var thisFieldError = errors[i].fieldErrors[fieldError];
	                                for(var j=0; j < thisFieldError.length; j++) {
	                                    message += (message.length > 0 ? '\n' : '') + thisFieldError[j].message;
	                                    if (errors[i].pageErrors[j]) {
	                                    	message = errors[i].pageErrors[j].message;
	                                    }
	                                }
                        		}
                            }
                        }
                        if(errors[i].message) {
                            message += (message.length > 0 ? '\n' : '') + errors[i].message;
                            message = errors[i].message;
                        }
                    }
                } else {
                    message += (message.length > 0 ? '\n' : '') + 'Unknown error';
                    message = 'Unknown error';
                }
                
                if (saveResult.state === "INCOMPLETE") {
                    console.log("Probleme de connexion.");
                    component.set("v.errorMsg","Probleme de connexion.");
                    component.set("v.showErrorMsg","true");
        			component.set("v.showSuccesMsg","false");
                } 
                if (saveResult.state === "ERROR") {
                    console.log('Probleme d\'enregistrement, erreur: ' + JSON.stringify(saveResult.error));
                    component.set("v.errorMsg",message);
                    component.set("v.showErrorMsg","true");
        			component.set("v.showSuccesMsg","false");
                } 
                else {
                    console.log('Probleme inconnu, etat: ' + saveResult.state + ', erreur: ' + JSON.stringify(saveResult.error));
                    component.set("v.errorMsg",'Probleme inconnu, etat: ' + saveResult.state + ', erreur: ' + message);
                    component.set("v.showErrorMsg","true");
        			component.set("v.showSuccesMsg","false");
                }
            }
        }));
        
        
    },
    
    handleDiscardChanges: function(component, event, helper) {
        component.find("recordHandler").reloadRecord();
        component.set("v.editModeOn","false");
    }  
    
})